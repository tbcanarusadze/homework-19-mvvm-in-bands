package com.example.homework19

import retrofit2.http.Body

interface CustomCallback {
    fun onFailure(response: String)
    fun onResponse(response: String)
}