package com.example.homework19

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


object HttpRequest {

    const val BANDS = "5ec3ab0f300000850039c29e"
    const val INFO = "5ec3ca1c300000e5b039c407"
    const val pirveliLinki = "http://www.mocky.io/v2/"

    private var retrofit = Retrofit.Builder()
        .baseUrl(pirveliLinki)
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(ApiService::class.java)

    interface ApiService {
        @GET("{path}")
        fun getRequest(@Path("path") competitions: String): Call<String>
    }

    fun getRequest(path: String, callback: CustomCallback){
        val call = service.getRequest(path)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback: CustomCallback) = object : Callback<String>{
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            callback.onResponse(response.body().toString())

        }

    }
}