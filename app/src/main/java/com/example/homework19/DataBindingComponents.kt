package com.example.homework19

import android.app.Activity
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object DataBindingComponents {
    @JvmStatic
    @BindingAdapter("setResource")
    fun setImage(view: ImageView, imageUrl : String){
        Glide.with(view).load(imageUrl).into(view)
    }
}