package com.example.homework19

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.homework19.databinding.BandsRecyclerviewLayoutBinding
import kotlinx.android.synthetic.main.bands_recyclerview_layout.view.*

class BandsRecyclerViewAdapter(
    private val items: List<BandsModel>
) :
    RecyclerView.Adapter<BandsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: BandsRecyclerviewLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.bands_recyclerview_layout,
            parent,
            false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind()

    override fun getItemCount() = items.size


    inner class ViewHolder(private val binding: BandsRecyclerviewLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.bandsModel = items[adapterPosition]
        }
    }
}

