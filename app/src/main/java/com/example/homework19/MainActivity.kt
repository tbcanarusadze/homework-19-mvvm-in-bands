package com.example.homework19

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework19.HttpRequest.BANDS
import com.example.homework19.databinding.BandsRecyclerviewLayoutBinding
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: BandsRecyclerViewAdapter
    private var listOfBands = ArrayList<BandsModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {

        adapter = BandsRecyclerViewAdapter(listOfBands)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        loadingTextView.visibility = View.VISIBLE
        request(BANDS)

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                adapter.notifyDataSetChanged()
                loadingTextView.visibility = View.GONE

            }, 2000)
        }

    }

    private fun refresh() {
        adapter.notifyDataSetChanged()
        loadingTextView.visibility = View.VISIBLE
    }

    private fun request(path: String) {
        HttpRequest.getRequest(path, object : CustomCallback {
            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String) {
                parseJson(response)
                adapter.notifyDataSetChanged()
                loadingTextView.visibility = View.GONE

            }
        })
    }


    private fun parseJson(response: String) {
        val json = JSONArray(response)
        for (item in 0 until json.length()) {
            val data = json[item] as JSONObject
            val bands = BandsModel()
            bands.name = data.getString("name")
            bands.imageUrl = data.getString("img_url")
            bands.info = data.getString("info")
            bands.genre = data.getString("genre")
            listOfBands.add(bands)
        }


    }
}
